@extends('app')

@section('content')

    <h1>Last played games</h1>
    <div class="col-md-12">
    <div class="col-md-12">
        <table class="table table-bordered">
            <tr>
                    <th>Team 1</th>
                    <th>Team 2</th>
                    <th>Sets</th>

            </tr>
            @foreach($games as $game)
                <tr>
                    <td>{{$game->team_1->toString()}}</td>
                    <td>{{$game->team_2->toString()}}</td>
                    <td>{{$game->toString('sets')}}</td>

                </tr>
            @endforeach
        </table>
    </div>
    </div>
@stop