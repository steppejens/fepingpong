@extends('app')

@section('content')
    <h1 class="page-heading">Register new match. </h1>
    <div class="row">
    {!! Form::open(['method' => 'POST', 'action' => 'GameController@confirm']) !!}
    @include('errors.error_list')

    <div class="col-md-12">

        <div class="col-md-6">
                    <h3>Team 1</h3>
            <div class="form-group">
                {!! Form::label('team_1_player_1', 'Player 1') !!}
                {!! Form::select('team_1_player_1',$players, null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('team_1_player_2', 'Player 2') !!}
                {!! Form::select('team_1_player_2', $players,null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-md-6">
                <h3>Team 2</h3>
             <div class="form-group">
                {!! Form::label('team_2_player_1', 'Player 1') !!}
                {!! Form::select('team_2_player_1',$players, null, ['class' => 'form-control']) !!}
             </div>
             <div class="form-group">
                 {!! Form::label('team_2_player_2', 'Player 2') !!}
                 {!! Form::select('team_2_player_2',$players, null, ['class' => 'form-control']) !!}
             </div>
        </div>
    </div>


    <div class="col-md-12 scoring">
            <div class="col-md-6">
                    <h3>Points</h3>
                <div class="form-group ">
                <h4>Set 1</h4>
                    {!! Form::text('set_1_team_1', null, ['class' => 'form-control setpoints', 'placeholder' => 'Team 1']) !!}
                    {!! Form::text('set_1_team_2', null, ['class' => 'form-control setpoints', 'placeholder' => 'Team 2']) !!}
                </div>

                <div class="form-group ">
                    <h4>Set 2</h4>
                    {!! Form::text('set_2_team_1', null, ['class' => 'form-control setpoints', 'placeholder' => 'Team 1']) !!}
                    {!! Form::text('set_2_team_2', null, ['class' => 'form-control setpoints', 'placeholder' => 'Team 2']) !!}
                </div>
                <div class="form-group ">
                    <h4>Set 3</h4>
                    {!! Form::text('set_3_team_1', null, ['class' => 'form-control setpoints', 'placeholder' => 'Team 1']) !!}
                    {!! Form::text('set_3_team_2', null, ['class' => 'form-control setpoints', 'placeholder' => 'Team 2']) !!}
                </div>
                </div>

            </div>


    <div class="col-md-12">
        <div class="col-md-12">

            <div class="form-group">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            </div>
         </div>
    </div>
    </div>

    {!! Form::close() !!}
@endsection