@extends('app')

@section('content')
    <div class="col-md-12">
        <h1>Players</h1>
        <div class="addPlayer">
            <a href="/players/add">Add player</a>
        </div>
          <table class="table table-bordered">
                    <tr>
                            <th>Player</th>
                            <th>Actions</th>
                    </tr>
                    @foreach($players as $player)
                        <tr>
                            <td>{{$player->name}}</td>
                            <td><a href="{{$player->id}}/view">View</a></td>
                        </tr>
                    @endforeach
                </table>
    </div>

@endsection