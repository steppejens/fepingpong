@extends('app')

@section('content')
    <h1 class="page-heading">Create new player </h1>
    <div class="row">
    {!! Form::open(['method' => 'POST', 'action' => 'PlayerController@addPlayerConfirm']) !!}
    @include('errors.error_list')

    <div class="col-md-12">

        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('name', 'Name') !!}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>

    </div>

    </div>

    {!! Form::close() !!}
@endsection