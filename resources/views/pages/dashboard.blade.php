@extends('app')

@section('content')

@if(Session::has('message'))
    <p class="alert alert-success">{{ Session::get('message') }}</p>
@endif

    <h1>Dashboard</h1>
    <h5 style="color: red">Elo ratings draaien nog even in test. Worden nog niet mee genomen in de ranking. </h5>
    <div class="col-md-12">
    <div class="col-md-12">
        <table class="table table-bordered">
            <tr>
                    <th></th>
                    <th>Player</th>
                    <th>Won</th>
                    <th>Lost</th>
                    <th>Sets Won</th>
                    <th>Sets Lost</th>
                    <th>Win ratio</th>
                    <th>Elo rating</th>
            </tr>
            @foreach($stats as $index => $stat)
                <tr>
                    <td>{{$index + 1}}</td>
                    <td>{{$stat->player->name}}</td>
                    <td>{{$stat->games_won}}</td>
                    <td>{{$stat->games_lost}}</td>
                    <td>{{$stat->sets_won}}</td>
                    <td>{{$stat->sets_lost}}</td>
                    <td>{{round($stat->games_ratio * 100) }}%</td>
                    <td>{{$stat->player->elo}}</td>
                </tr>
            @endforeach
        </table>
    </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-3">
            <h3>Most games won</h3>
            <table class="table table-bordered">
                    <tr><th></th><th>Name</th></tr>
            @foreach($mostGamesWon as $index => $stat)
                    <tr><td>{{$index + 1}}</td><td> {{$stat->player->name}}</td></tr>
            @endforeach
            </table>
        </div>
        <div class="col-md-3">
            <h3>Most sets won</h3>
            <table class="table table-bordered">
                    <tr><th></th><th>Name</th></tr>
            @foreach($mostSetsWon as $index => $stat)
                    <tr><td>{{$index + 1}}</td><td> {{$stat->player->name}}</td></tr>
            @endforeach
            </table>
        </div>
        <div class="col-md-3">
                    <h3>Best win ratio</h3>
                    <table class="table table-bordered">
                            <tr><th></th><th>Name</th></tr>
                    @foreach($bestRatio as $index => $stat)
                            <tr><td>{{$index + 1}}</td><td> {{$stat->player->name}}</td></tr>
                    @endforeach
                    </table>
        </div>
        <div class="col-md-3">
                            <h3>Longest win streak</h3>
                            <table class="table table-bordered">
                                    <tr><th></th><th>Name</th></tr>
                            @foreach($longestWinStreak as $index => $stat)
                                    <tr><td>{{$index + 1}}</td><td> {{$stat->player->name}} ({{$stat->longest_win_streak}})</td></tr>
                            @endforeach
                            </table>
                </div>
    </div>
@stop