<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('sets', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('points_team_1');
            $table->integer('points_team_2');

            $table->integer('won_by')->unsigned();
            $table->integer('team_1_id')->unsigned();
            $table->integer('team_2_id')->unsigned();
            $table->integer('game_id')->unsigned();
            $table->timestamps();

            $table->foreign('team_1_id')->references('id')->on('teams');
            $table->foreign('team_2_id')->references('id')->on('teams');
            $table->foreign('won_by')->references('id')->on('teams');
            $table->foreign('game_id')->references('id')->on('games');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('sets');
	}

}
