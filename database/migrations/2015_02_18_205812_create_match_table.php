<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('games', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('team_1_id')->unsigned();
            $table->integer('team_2_id')->unsigned();
            $table->integer('won_by')->unsigned()->nullable();


            $table->foreign('team_1_id')->references('id')->on('teams');
            $table->foreign('team_2_id')->references('id')->on('teams');
            $table->foreign('won_by')->references('id')->on('teams');
            $table->timestamps();
        });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('games');
	}

}
