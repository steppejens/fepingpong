<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerStatistics extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        Schema::create('player_statistics', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            // //calculated statistics
            $table->integer('current_win_streak')->default(0);
            $table->integer('current_lose_streak')->default(0);
            $table->integer('longest_win_streak')->default(0);
            $table->integer('longest_lose_streak')->default(0);
            $table->integer('games_won')->default(0);
            $table->integer('games_lost')->default(0);
            $table->double('games_ratio')->default(0);
            $table->integer('sets_won')->default(0);
            $table->integer('sets_lost')->default(0);

            $table->integer('player_id')->unsigned();
            $table->foreign('player_id')->references('id')->on('players');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('player_statistics');
	}

}
