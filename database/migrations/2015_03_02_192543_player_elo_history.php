<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlayerEloHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('player_elo_history', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->double('new_elo');
            $table->integer('player_id')->unsigned();
            $table->integer('game_id')->unsigned();

            $table->foreign('player_id')->references('id')->on('players');
            $table->foreign('game_id')->references('id')->on('games');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('player_elo_history');
	}

}
