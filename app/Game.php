<?php namespace FePingPong;

use Illuminate\Database\Eloquent\Model;

class Game extends Model {

    protected $table = "games";

    //Games has many sets
    public function sets(){
        return $this->hasMany('FePingPong\Set');
    }

    public function team_1(){
        return $this->belongsTo('FePingPong\Team','team_1_id');
    }

    public function team_2(){
        return $this->belongsTo('FePingPong\Team','team_2_id');
    }

    public function toString($what){
        if($what == 'sets'){
            $team1Won = 0;
            $team2Won = 0;
            foreach($this->sets as $set){
                if($set->won_by == $this->team_1->id){
                    $team1Won++;
                }else{
                    $team2Won++;
                }
            }

            return $team1Won . ' / ' . $team2Won;
        }

        return '';
    }

    public function updateStatisticsAndSave(){
        $team_1 = $this->team_1;
        $team_2 = $this->team_2;

        $team_1_elo = array(array("id" =>  $team_1->player_1->id, "elo" => $team_1->player_1->elo),array("id" =>  $team_1->player_2->id, "elo" => $team_1->player_2->elo));
        $team_2_elo = array(array("id" =>  $team_2->player_1->id, "elo" => $team_2->player_1->elo),array("id" =>  $team_2->player_2->id, "elo" => $team_2->player_2->elo));

        $eloCalulator = new Elo();
        $eloCalulator->setTeams($team_1_elo, $team_2_elo);
        $calculatedElo = $eloCalulator->calculate();

        if($this->won_by == $team_1->id){
            $team_1->addVictory($this->calculateSetsWonByTeam($team_1), $this->calculateSetsLostByTeam($team_1));
            $team_2->addLoss($this->calculateSetsWonByTeam($team_2), $this->calculateSetsLostByTeam($team_2));

            $team_1->player_1->saveNewElo($calculatedElo['team1'][0]['gain'], $this->id);
            $team_1->player_2->saveNewElo($calculatedElo['team1'][1]['gain'], $this->id);
            $team_2->player_1->saveNewElo(-1 * $calculatedElo['team2'][0]['loss'], $this->id);
            $team_2->player_2->saveNewElo(-1 * $calculatedElo['team2'][1]['loss'], $this->id);


        }else{
            $team_2->addVictory($this->calculateSetsWonByTeam($team_2), $this->calculateSetsLostByTeam($team_2));
            $team_1->addLoss($this->calculateSetsWonByTeam($team_1), $this->calculateSetsLostByTeam($team_1));

            $team_1->player_1->saveNewElo(-1 * $calculatedElo['team1'][0]['loss'], $this->id);
            $team_1->player_2->saveNewElo(-1 * $calculatedElo['team1'][1]['loss'], $this->id);
            $team_2->player_1->saveNewElo($calculatedElo['team2'][0]['gain'], $this->id);
            $team_2->player_2->saveNewElo($calculatedElo['team2'][1]['gain'], $this->id);
        }



        $this->save();
    }

    private function calculateSetsWonByTeam($team){
        $wonSets = 0;
        foreach($this->sets as $set) {
            if ($set->won_by== $team->id) {
                $wonSets++;
            }
        }
        return $wonSets;
    }

    private function calculateSetsLostByTeam($team)
    {
        $lostSets = 0;
        foreach ($this->sets as $set) {
            if ($set->won_by != $team->id) {
                $lostSets++;
            }
        }
        return $lostSets;
    }

}
