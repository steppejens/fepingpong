<?php namespace FePingPong;

use Illuminate\Database\Eloquent\Model;

class Player extends Model {

    protected $table = "players";


	public function teams(){
        $this->hasMany('FePingPong\Set');
    }

    public function saveNewElo($newElo, $gameId){
        $this->elo += $newElo;
        $this->save();

        $eloHistory = new EloHistory();
        $eloHistory->saveNewEloHistory($newElo,$gameId, $this->id);
    }


}
