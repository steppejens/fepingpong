<?php namespace FePingPong;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model {

    protected $table = "player_statistics";


    public function player(){
        return $this->belongsTo('FePingPong\Player');
    }

}
