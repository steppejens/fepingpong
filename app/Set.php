<?php namespace FePingPong;

use Illuminate\Database\Eloquent\Model;

class Set extends Model {

    protected $table = "sets";

    //A set has 1 match
    public function game(){
       return $this->belongsTo('FePingPong/Game');
    }

    public function team_1(){
       return $this->belongsTo('FePingPong\Team', 'team_1_id');
    }

    public function team_2(){
       return $this->belongsTo('FePingPong\Team', 'team_2_id');
    }

    public function won_by(){
       return $this->belongsTo('FePingPong\Team', 'won_by');
    }

}
