<?php namespace FePingPong;

use Illuminate\Database\Eloquent\Model;

class EloHistory extends Model {


    protected $table = "player_elo_history";

    public function Game(){
        return $this->belongsTo('FePingPong\Game','game_id');
    }

    public function saveNewEloHistory($newElo, $gameId, $playerId){

        $this->new_elo = $newElo;
        $this->game_id = $gameId;
        $this->player_id = $playerId;
        $this->save();
    }

}
