<?php namespace FePingPong\Http\Requests;

use FePingPong\Http\Requests\Request;
use FePingPong\Player;
use Illuminate\Support\Facades\Auth;

class CreatePlayerRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
        if( Auth::id() == 1){ return TRUE;}
		return FALSE;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required'
		];
	}
//This is even the correct way?
    public function getValidatorInstance() {
        $validator = parent::getValidatorInstance();

        $validator->after(function() use ($validator) {
            // Do check here
            $uniqueName =  Player::where('name', $this->request->get('name'))->count();
            if($uniqueName > 0) {
                $validator->errors()->add('name', 'A player with this name already exists!');
            }
        });

        return $validator;
    }

}
