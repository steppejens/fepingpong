<?php namespace FePingPong\Http\Requests;

use FePingPong\Http\Requests\Request;



class CreateGameRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $input = Request::all();

		$rules =  array(
            'team_1_player_1' => 'required',
            'team_2_player_1' => 'required',
            'set_1_team_1' => 'required|numeric',
            'set_1_team_2' => 'required|numeric',
            'set_2_team_1' => 'required|numeric',
            'set_2_team_2' => 'required|numeric',
        );

        $team1player2 = $input['team_1_player_2'];

        if(!empty($team1player2) ){
            $rules['team_2_player_2'] = 'required';
        }

        $team2player2 = $input['team_2_player_2'];

        if(!empty($team2player2)){
            $rules['team_1_player_2'] = 'required';
        }

        return $rules;
	}

}
