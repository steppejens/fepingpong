<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Dashboard
Route::get('/', 'DashboardController@dashboard');

//Players
Route::get('/players', 'PlayerController@listPlayers');
Route::get('/players/add', 'PlayerController@addPlayer');
Route::post('/players/add/confirm', 'PlayerController@addPlayerConfirm');

//Games
Route::resource('games', 'GameController');
Route::post('/games/confirm', 'GameController@confirm');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

