<?php namespace FePingPong\Http\Controllers;

use FePingPong\Http\Requests;
use FePingPong\Http\Controllers\Controller;

use FePingPong\Statistics;
use Illuminate\Http\Request;

class DashboardController extends Controller {

	public function dashboard(){
        $statistics = Statistics::orderBy('games_won', 'ascending')->orderBy('games_lost', 'asc')->orderBy('sets_won', 'ascending')->get();
        $mostGamesWon = Statistics::orderBy('games_won', 'ascending')->orderBy('games_lost', 'asc')->take(3)->get();
        $mostSetsWon = Statistics::orderBy('sets_won', 'ascending')->orderBy('sets_lost', 'asc')->take(3)->get();
        $bestRatio = Statistics::orderBy('games_ratio', 'ascending')->take(3)->get();
        $longestWinStreak = Statistics::orderBy('longest_win_streak', 'ascending')->take(3)->get();

        return view('pages.dashboard', array('stats' => $statistics,'mostGamesWon' => $mostGamesWon,'mostSetsWon' => $mostSetsWon, 'bestRatio' => $bestRatio, 'longestWinStreak' => $longestWinStreak));
    }

}
