<?php namespace FePingPong\Http\Controllers;

use FePingPong\Http\Requests;
use FePingPong\Http\Controllers\Controller;

use FePingPong\Http\Requests\CreatePlayerRequest;
use FePingPong\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class PlayerController extends Controller {

	public function listPlayers()
    {
        $players = Player::all();

        return view('players.list', compact('players',$players));
    }

    public function addPlayer(){

        return view('players.createPlayer');

    }


    public function addPlayerConfirm(CreatePlayerRequest $request){
        $name = $request->input('name');

        $player = new Player();
        $player->name = $name;
        $player->save();

        //TODO How to display flash messages
        return redirect('/')->with('message', $name . ' added!');


    }

}
