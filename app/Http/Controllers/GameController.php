<?php namespace FePingPong\Http\Controllers;

use FePingPong\Game;
use FePingPong\Http\Requests;
use FePingPong\Http\Controllers\Controller;

use FePingPong\Http\Requests\CreateGameRequest;
use FePingPong\Player;
use FePingPong\Set;
use FePingPong\Team;
use Illuminate\Http\Request;

class GameController extends Controller {

	public function index(){

        $games = Game::orderBy('created_at', 'desc')->get();

        return view('games.list', compact('games',$games));
    }

    public function create(){
        //get list of players
        $players = Player::lists('name', 'id');
        $players = ['0' => 'Select player'] + $players;

        return view('games.create', compact('players',$players));

    }

    public function confirm(CreateGameRequest $request){

        //get list of players
        $team1player1 = $request->input('team_1_player_1');
        $team1player2 = $request->input('team_1_player_2');

        $team2player1 = $request->input('team_2_player_1');
        $team2player2 = $request->input('team_2_player_2');

        //Extract and refactor later.
        $team1 = Team::findTeam($team1player1,$team1player2);
        if(empty($team1)){
            $team1 = new Team();
            $team1->player_1_id = $team1player1;
            $team1->player_2_id = $team1player2;
            $team1->createKey($team1player1, $team1player2);
            $team1->save();
        };

        $team2 = Team::findTeam($team2player1,$team2player2);
        if(empty($team2)){
            $team2 = new Team();
            $team2->player_1_id = $team2player1;
            $team2->player_2_id = $team2player2;
            $team2->createKey($team2player1, $team2player2);
            $team2->save();
        };
        $game = new Game();
        $game->team_1_id = $team1->id;
        $game->team_2_id = $team2->id;
        $game->save();


        $set1 = $this->createSet(1, $request, $team1, $team2,$game);
        $set2 = $this->createSet(2, $request, $team1, $team2,$game);
        $set3 = null;
        if($this->isSetPlayed(3, $request)) {
            $set3 = $this->createSet(3, $request, $team1, $team2, $game);
        }
        if(!empty($set3)){
            $game->won_by = $set3->won_by;
        }else{
            //What if only 2 sets are played, and both won 1?  Won_by most points scored?
            $game->won_by = $set2->won_by;
        }

        $game->updateStatisticsAndSave();


        return redirect('/')->with('message', 'Game registered!');

    }


    public function createSet($setNumber, CreateGameRequest $request, Team $team1, Team $team2, Game $game){
        $set = new Set();
        $set->points_team_1 = $request->input('set_'.$setNumber.'_team_1');
        $set->points_team_2 = $request->input('set_'.$setNumber.'_team_2');
        if($set->points_team_1 > $set->points_team_2){
            $set->won_by = $team1->id;
        }else{
            $set->won_by = $team2->id;
        }
        $set->team_1_id = $team1->id;
        $set->team_2_id = $team2->id;
        $set->game_id = $game->id;
        $set->save();
        return $set;
    }


    private function isSetPlayed($setNumber, CreateGameRequest $request){

        $points_team_1 = $request->input('set_'.$setNumber.'_team_1');
        return !empty($points_team_1);
    }


}
