<?php namespace FePingPong;

use FePingPong\Statistics;
use Illuminate\Database\Eloquent\Model;

class Team extends Model {

    protected $table = "teams";


    public function player_1(){
        return $this->belongsTo('FePingPong\Player', 'player_1_id');
    }

    public function player_2(){
        return $this->belongsTo('FePingPong\Player', 'player_2_id');
    }

    public static function findTeam($player1_id, $player2_id)
    {
        return Team::where('team_key', $player1_id.'_'.$player2_id)->orWhere('team_key', $player2_id.'_'.$player1_id)->first();
    }

    public function createKey($key1, $key2){
        $this->team_key = $key1 . '_' . $key2;
    }

    public function toString(){
        return $this->player_1->name . ' / '. $this->player_2->name;
    }

    public function addVictory($setsWon, $setsLost){
        $player_1_stats = Statistics::where('player_id', $this->player_1_id)->first();
        if(empty($player_1_stats)){
            $player_1_stats = new Statistics();
            $player_1_stats->player_id = $this->player_1_id;
            $player_1_stats->save();
        }
        $player_1_stats->current_win_streak += 1;
        if($player_1_stats->current_win_streak > $player_1_stats->longest_win_streak) {
            $player_1_stats->longest_win_streak = $player_1_stats->current_win_streak;
        }
        $player_1_stats->current_lose_streak = 0;
        $player_1_stats->games_won += 1;
        $player_1_stats->games_ratio = $player_1_stats->games_won / ($player_1_stats->games_won + $player_1_stats->games_lost);
        $player_1_stats->sets_lost += $setsLost;
        $player_1_stats->sets_won += $setsWon;
        $player_1_stats->save();

        

        $player_2_stats = Statistics::where('player_id', $this->player_2_id)->first();
        if(empty($player_2_stats)){
            $player_2_stats = new Statistics();
            $player_2_stats->player_id = $this->player_2_id;
            $player_2_stats->save();
        }
        $player_2_stats->current_win_streak += 1;
        if($player_2_stats->current_win_streak > $player_2_stats->longest_win_streak) {
            $player_2_stats->longest_win_streak = $player_2_stats->current_win_streak;
        }
        $player_2_stats->current_lose_streak = 0;
        $player_2_stats->games_won += 1;
        $player_2_stats->games_ratio = $player_2_stats->games_won / ($player_2_stats->games_won + $player_2_stats->games_lost);
        $player_2_stats->sets_lost += $setsLost;
        $player_2_stats->sets_won += $setsWon;
        $player_2_stats->save();
    }

    public function addLoss($setsWon, $setsLost){
        $player_1_stats = Statistics::where('player_id', $this->player_1_id)->first();
        if(empty($player_1_stats)){
            $player_1_stats = new Statistics();
            $player_1_stats->player_id = $this->player_1_id;
            $player_1_stats->save();
        }
        $player_1_stats->current_lose_streak += 1;
        if($player_1_stats->current_lose_streak > $player_1_stats->longest_lose_streak) {
            $player_1_stats->longest_lose_streak = $player_1_stats->current_lose_streak;
        }
        $player_1_stats->current_win_streak = 0;
        $player_1_stats->games_lost += 1;
        $player_1_stats->games_ratio = $player_1_stats->games_won / ($player_1_stats->games_won + $player_1_stats->games_lost);
        $player_1_stats->sets_lost += $setsLost;
        $player_1_stats->sets_won += $setsWon;
        $player_1_stats->save();

        $player_2_stats = Statistics::where('player_id', $this->player_2_id)->first();
        if(empty($player_2_stats)){
            $player_2_stats = new Statistics();
            $player_2_stats->player_id = $this->player_2_id;
            $player_2_stats->save();
        }
        $player_2_stats->current_lose_streak += 1;
        if($player_2_stats->current_lose_streak > $player_2_stats->longest_lose_streak) {
            $player_2_stats->longest_lose_streak = $player_2_stats->current_lose_streak;
        }
        $player_2_stats->current_win_streak = 0;
        $player_2_stats->games_lost += 1;
        $player_2_stats->games_ratio = $player_2_stats->games_won / ($player_2_stats->games_won + $player_2_stats->games_lost);
        $player_2_stats->sets_lost += $setsLost;
        $player_2_stats->sets_won += $setsWon;
        $player_2_stats->save();
    }


}
